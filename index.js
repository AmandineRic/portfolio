const menuBurger = document.getElementById("menu-burger");
const menuList = document.getElementById("menu-list");
const nav = document.getElementById("nav");

menuBurger.addEventListener("click", () => {
  menuList.classList.toggle("toggle-nav");
  nav.classList.toggle("expand-nav");
  window.USER_IS_TOUCHING = true;
});
