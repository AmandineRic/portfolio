# Portfolio

A fully responsive portfolio made with vanilla HTML, CSS and JS. 👩‍💻

The visitor can see why he should hire me. :muscle:

The visitor can see my projects. :mag:

The visitor can click on my projects to see them in action. 🖱️

The visitor can see my informations to contact me. ☎️

<img src="img/maqueta.jpg"></img>
